from tqdm import tqdm
from words.models import Word
from words.models import Kanji
from xml.dom import minidom

def init_words():

    doc = minidom.parse("dictionary/assets/JMdict_e.xml")

    entries = doc.getElementsByTagName("entry")
    count = 0

    for entry in tqdm(entries):
        reading = entry.getElementsByTagName("reb")[0].firstChild.data 
        try:
            expression = entry.getElementsByTagName("keb")[0].firstChild.data
        except:
            expression = reading 
            
        definition = entry.getElementsByTagName("gloss")[0].firstChild.data

        _, created = Word.objects.get_or_create(
            expression=expression,
            defaults={
                'reading': reading,
                'definition': definition
            }
        )
        if created: 
            count+=1
    print('Done. Created:', count)

def init_kanji():

    doc = minidom.parse("dictionary/assets/kanjidic2.xml")

    entries = doc.getElementsByTagName("character")
    count = 0

    for entry in tqdm(entries):
        readings = entry.getElementsByTagName("reading")
        reading_kun = ""
        reading_on = ""

        for reading in readings:
            if reading.getAttribute("r_type") == "ja_kun":
                if reading_kun != "":
                    reading_kun += ", "
                reading_kun += reading.firstChild.data   
            elif reading.getAttribute("r_type") == "ja_on":
                if reading_on != "":
                    reading_on += ", "
                reading_on += reading.firstChild.data   
                    
        literal = entry.getElementsByTagName("literal")[0].firstChild.data

        try:
            definition = entry.getElementsByTagName("meaning")[0].firstChild.data
        except IndexError:
            definition = "radical (no definition)"

        _, created = Kanji.objects.get_or_create(
            literal=literal, defaults={
                'reading_on': reading_on,
                'reading_kun': reading_kun,
                'definition': definition,    
            }        
        )
        if created: 
            count+=1
    print('Done. Created:', count) 
