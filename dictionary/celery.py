import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dictionary.settings')

BASE_REDIS_URL = os.environ.get('REDIS_URL', 'redis://localhost:6379')

app = Celery('dictionary')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()

app.conf.broker_url = BASE_REDIS_URL

app.conf.beat_scheduler = 'django_celery_beat.schedulers.DatabaseScheduler'

app.conf.timezone = 'Europe/Warsaw'

# app.conf.beat_schedule = {
#       'add-every-thursday-and-sunday': {
#         'task': 'tasks.add',
#         'schedule': crontab(minute="30", hour='2', day_of_week='thu,sun'),
#         'args': (16, 16),
#     },
# }

