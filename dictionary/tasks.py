from celery import shared_task
from dictionary._scripts import init_kanji, init_words

@shared_task(name="run_scripts")
def run_scripts():
    init_words()
    init_kanji()
    