from django.contrib import admin
from words.models import Comment, Kanji, Word

# Register your models here.
admin.site.register(Word)
admin.site.register(Kanji)
admin.site.register(Comment)
