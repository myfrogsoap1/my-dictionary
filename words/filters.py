from django_filters import rest_framework as filters
from .models import Word, Kanji

class WordFilterSet(filters.FilterSet):

    class Meta:
        model = Word
        fields = {
            'expression': ['iexact']
        }

class KanjiFilterSet(filters.FilterSet):

    class Meta:
        model = Kanji
        fields = {
            'literal': ['iexact']
        }       
