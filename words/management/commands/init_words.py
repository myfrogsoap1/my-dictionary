from django.core.management.base import BaseCommand
from dictionary._scripts import init_words

class Command(BaseCommand):
    def handle(self, *args, **options):

        init_words()

  