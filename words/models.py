from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

class Word(models.Model):
    expression = models.CharField(max_length=100, unique=True)
    reading = models.CharField(max_length=100)
    definition = models.CharField(max_length=100)
    
    def __str__(self):
        return self.expression

class Kanji(models.Model):
    literal = models.CharField(max_length=100, unique=True)
    reading_on = models.CharField(max_length=100, null=False, blank=False)
    reading_kun = models.CharField(max_length=100, null=False, blank=False)
    definition = models.CharField(max_length=100)
    
    def __str__(self):
        return self.literal     

class Comment(models.Model):
    word = models.ForeignKey(
        Word, related_name='comments', on_delete=models.CASCADE
    )
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comments')
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    edited = models.BooleanField(default=False)
    reply_to = models.OneToOneField(to="Comment", on_delete=models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return "'%s' by '%s'" % (self.id, self.owner)

    class Meta:
        ordering = ('timestamp', 'id')
