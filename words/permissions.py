from rest_framework.permissions import BasePermission, SAFE_METHODS

class IsCommentOwnerOrReadOnly(BasePermission):

      def has_permission(self, request, view):
        comment = None
        owner = False
        if view.kwargs.get('pk'):
            try:
                comment = view.get_queryset().get(id=view.kwargs.get('pk'))
            except view.queryset.model.DoesNotExist:
                return False
            return bool(comment.owner == request.user)
        if bool(request.user and request.user.is_authenticated):
            read_only = request.method in (*SAFE_METHODS, 'POST') 
        else:
            read_only = request.method in SAFE_METHODS   
        return bool(
            read_only and request.user or owner
        )

class IsAdminOrReadOnly(BasePermission):
    
    def has_permission(self, request, view):
        return bool(
            request.method in SAFE_METHODS or
            request.user and
            request.user.is_staff
        )
