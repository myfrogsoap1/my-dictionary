from rest_framework import serializers
from .models import Kanji, Word, Comment

class WordSerializer(serializers.ModelSerializer):

    class Meta:
        model = Word
        fields = '__all__'

class KanjiSerializer(serializers.ModelSerializer):

    class Meta:
        model = Kanji
        fields = '__all__'   

class CommentSerializer(serializers.ModelSerializer):
   
    class Meta:
        model = Comment
        fields = '__all__'     
