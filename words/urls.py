from django.urls import path
from django.conf.urls import include
from rest_framework_nested import routers
from . import views

router = routers.DefaultRouter()
router.register(r"kanji", views.KanjiAPIViewset)
router.register(r"", views.WordAPIViewset)

words_router = routers.NestedDefaultRouter(router, r"", lookup="words")
words_router.register(r"comments", views.CommentAPIViewset)

urlpatterns = [
    path(r"", include(router.urls)),
    path(r"", include(words_router.urls)),
]
