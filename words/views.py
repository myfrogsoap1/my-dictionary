from rest_framework import viewsets
from rest_framework.response import Response
from django.http import QueryDict
from .models import Word, Kanji, Comment
from .serializers import WordSerializer, KanjiSerializer, CommentSerializer  
from .permissions import IsCommentOwnerOrReadOnly, IsAdminOrReadOnly
from .filters import WordFilterSet, KanjiFilterSet

class WordAPIViewset(viewsets.ModelViewSet):
    queryset = Word.objects.all()
    serializer_class = WordSerializer
    permission_classes = [IsAdminOrReadOnly]
    search_fields = ['expression', 'reading','definition']
    filterset_class = WordFilterSet
    lookup_field = 'expression'

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)  
        comments_queryset = Comment.objects.filter(word=instance)[:10]
        comments_data = CommentSerializer(comments_queryset, many=True).data
        expression=self.kwargs['expression']
        expression_list = list(expression)
        kanji_queryset = Kanji.objects.filter(literal__in=expression_list)
        kanji_data = KanjiSerializer(kanji_queryset, many=True).data
        data = {
            **serializer.data,
            "comments": comments_data,
            "kanji": kanji_data,
        }
        return Response(data)

class KanjiAPIViewset(viewsets.ModelViewSet):
    queryset = Kanji.objects.all()
    serializer_class = KanjiSerializer  
    permission_classes = [IsAdminOrReadOnly]  
    search_fields = ['literal', 'reading_on','reading_kun','definition']
    filterset_class = KanjiFilterSet
    lookup_field = 'literal'

class CommentAPIViewset(viewsets.ModelViewSet):
    permission_classes = [IsCommentOwnerOrReadOnly]
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer    
    search_fields = ['content']

    def get_queryset(self):
        return Comment.objects.filter(
            word__expression=self.kwargs['words_expression'],
        )

    def create(self, request, *args, **kwargs):
        if isinstance(request.data, QueryDict):
            request.data._mutable = True
        request.data['word'] = Word.objects.get(expression=self.kwargs['words_expression']).pk
        request.data['owner'] = request.user.pk
        return super().create(request, args, kwargs)
